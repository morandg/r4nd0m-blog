Title: whoami
Date: 2016-08-19 19:00
Category: News
Tags: me

I'm Guy Morand, aka [R4nd0m 6uy](http://r4nd0m6uy.ch) I grew up in
[Fribourg](http://www.fribourgtourisme.ch), Switzerland but live in
[Winterthur](https://winterthur.ch/).

# Job
I do some system/software architecture and write applications in C/C++.
I also take care of the
[Linux BSP](https://en.wikipedia.org/wiki/Board_support_package) and
tweak a bit the [Linux kernel](http://www.kernel.org)

More details about my profesional background can be found in my
[curriculum vitae](../documents/CV-EN.pdf)

# Hobbies
Of course, my job is one of the hobby that takes most of my time but I
also do other things.

## Skateboard
When the weather permits, I go out and go down the hill with my
skateboards. I have few at home but my favorite is a
[Penny](http://www.pennyskateboards.com/) alike that is made from wood.

## Video games
I like playing video games with a strong emphasis on retro gaming.

## Music
I wish I could call myself a musician but I mostly play for fun and to
relax. I learned keyboard when I was young and play with the old
keyboards I got from my father
[who is a real pianist](http://bulls-band.ch/).

I'm very glad to have some treasures like his old
[Fender Rhodes](https://en.wikipedia.org/wiki/Rhodes_piano) and his
[Yamaha DX-7](https://en.wikipedia.org/wiki/Yamaha_DX7). Unfortunately
they don't work as if they were brand new but I'm planning to fix that
issue soon or later!

## Beers
When well deserved, I enjoy drinking nicely chosen beer, my favorites are
the Belgium one, especially abbaye and dark beers.
[I sometime brew beer myself](https://github.com/r4nd0m6uy/GuixBeers).

# Spoken languages
My mother tongue is **French** but I can understand and speak a bit of
**English** and a bit less of **German**.

That's why you might see some mistakes or sentences that look
gramatically weird. In that case, do not hesitate to contact me, so I
can learn and fix them!

# Contact
You can reach me at [blog] [at] [r4nd0m6uy] [dot] [ch]
